# Configure the Microsoft Azure Provider
provider "azurerm" {
  version = "~> 0.3"
}
module "virtual_network" {
  source = "modules/virtual_network"
  location = "${var.location}"
  resource_group_name = "${module.resource_group.name}"
}
module "storage_account" {
  source = "modules/storage_account"
  location = "${var.location}"
  resource_group_id = "${module.resource_group.id}"
  resource_group_name = "${module.resource_group.name}"
  uid = "${module.unique_id.uid}"
}
module "virtual_machine" {
  source = "modules/virtual_machine"
  location = "${var.location}"
  vnetwork_interface_id = "${module.virtual_network.nic}"
  resource_group_name = "${module.resource_group.name}"
  blob_storage_url = "${module.storage_account.url}"
  sshkey = "${var.sshkey}"
  admin_username = "${var.admin_username}"
}
module "resource_group" {
  source = "modules/resource_group"
  location = "${var.location}"
  resource_group_name = "${var.resource_group_name}"
}
module "database" {
  source = "modules/database"
  location = "${var.location}"
  resource_group_name = "${module.resource_group.name}"
  vmip = "${module.virtual_network.vmip}"
  dblogin = "${var.dblogin}"
  dbpassword = "${var.dbpassword}"
  uid = "${module.unique_id.uid}"
}
module "unique_id" {
  source = "modules/unique_id"
  resource_group_name = "${module.resource_group.name}"
}
module "load_balancer" {
  source = "modules/load_balancer"
  location = "${var.location}"
  resource_group_name = "${module.resource_group.name}"
  subnet = "${module.virtual_network.subnet}"
  sshkey = "${var.sshkey}"
}
module "key_vault" {
  source = "modules/key_vault"
  location = "${var.location}"
  resource_group_name = "${module.resource_group.name}"
  tenant_id = "${var.tenant_id}"
  object_id = "${var.object_id}"
}
