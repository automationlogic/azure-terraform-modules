variable "location" {
  default = "East US"
}

variable "sshkey" {}

variable "dblogin" {}

variable "dbpassword" {}

variable "resource_group_name" {}

variable "admin_username" {}

variable "tenant_id" {}

variable "object_id" {}
