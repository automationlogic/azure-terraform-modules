resource "azurerm_lb" "mylb" {
  name                = "${var.resource_group_name}lb"
  location            = "${var.location}"
  resource_group_name = "${var.resource_group_name}"

  frontend_ip_configuration {
    name                 = "${var.frontend_name}"
    public_ip_address_id = "${azurerm_public_ip.my_lb_public_ip.id}"
  }

  tags {
      environment = "Terraform Demo"
  }
}

resource "azurerm_public_ip" "my_lb_public_ip" {
    name                         = "${var.resource_group_name}publicLBIP"
    location                     = "${var.location}"
    resource_group_name          = "${var.resource_group_name}"
    public_ip_address_allocation = "static"

    tags {
        environment = "Terraform Demo"
    }
}

resource "azurerm_lb_backend_address_pool" "mylb_backend_pool" {
  resource_group_name = "${var.resource_group_name}"
  loadbalancer_id     = "${azurerm_lb.mylb.id}"
  name                = "${var.backend_pool_name}"
}

resource "azurerm_lb_probe" "mylb_probe" {
  resource_group_name = "${var.resource_group_name}"
  loadbalancer_id     = "${azurerm_lb.mylb.id}"
  name                = "${var.probe_params[0]}"
  protocol            = "${var.probe_params[1]}"
  port                = "${var.probe_params[2]}"
  request_path        = "${var.probe_params[3]}"
  interval_in_seconds = "${var.probe_params[4]}"
  number_of_probes    = "${var.probe_params[5]}"
}

resource "azurerm_lb_rule" "mylb_rule" {
  resource_group_name            = "${var.resource_group_name}"
  loadbalancer_id                = "${azurerm_lb.mylb.id}"
  name                           = "${var.rule_params[0]}"
  protocol                       = "${var.rule_params[1]}"
  frontend_port                  = "${var.rule_params[2]}"
  backend_port                   = "${var.rule_params[3]}"
  enable_floating_ip             = "${var.rule_params[4]}"
  idle_timeout_in_minutes        = "${var.rule_params[5]}"
  frontend_ip_configuration_name = "${var.frontend_name}"
  backend_address_pool_id        = "${azurerm_lb_backend_address_pool.mylb_backend_pool.id}"
  probe_id                       = "${azurerm_lb_probe.mylb_probe.id}"
  depends_on                     = ["azurerm_lb_probe.mylb_probe"]
}

resource "azurerm_lb_nat_pool" "mylb_nat_pool" {
  count                          = "${var.nat_pool_params[0]}"
  name                           = "${var.nat_pool_params[1]}"
  protocol                       = "${var.nat_pool_params[2]}"
  frontend_port_start            = "${var.nat_pool_params[3]}"
  frontend_port_end              = "${var.nat_pool_params[4]}"
  backend_port                   = "${var.nat_pool_params[5]}"
  resource_group_name            = "${var.resource_group_name}"
  loadbalancer_id                = "${azurerm_lb.mylb.id}"
  frontend_ip_configuration_name = "${var.frontend_name}"
}

resource "azurerm_virtual_machine_scale_set" "mylb_scaleset" {
  name                = "${element(var.scale_set_params["name"], 0)}"
  upgrade_policy_mode = "${element(var.scale_set_params["upgrade_policy_mode"], 0)}"
  location            = "${var.location}"
  resource_group_name = "${var.resource_group_name}"

  sku {
    name     = "${element(var.scale_set_params["sku"], 0)}"
    tier     = "${element(var.scale_set_params["sku"], 1)}"
    capacity = "${element(var.scale_set_params["sku"], 2)}"
  }

  storage_profile_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04.0-LTS"
    version   = "latest"
  }

  storage_profile_os_disk {
    name              = ""
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Premium_LRS"
  }

  os_profile {
    computer_name_prefix = "${element(var.scale_set_params["os_profile"], 0)}"
    admin_username       = "${element(var.scale_set_params["os_profile"], 1)}"
    admin_password       = "${element(var.scale_set_params["os_profile"], 2)}"
  	custom_data          = "${base64encode(file("${path.module}/install_nginx.sh"))}"
  }

  os_profile_linux_config {
    disable_password_authentication = true

    ssh_keys {
      path     = "${element(var.scale_set_params["ssh_keys"], 0)}"
      key_data = "${file("${var.sshkey}")}"
    }
  }

  network_profile {
    name    = "MyScaleSetNetworkProfile"
    primary = true

    ip_configuration {
      name                                   = "TestIPConfiguration"
      subnet_id                              = "${var.subnet}"
      load_balancer_backend_address_pool_ids = ["${azurerm_lb_backend_address_pool.mylb_backend_pool.id}"]
      load_balancer_inbound_nat_rules_ids    = ["${element(azurerm_lb_nat_pool.mylb_nat_pool.*.id, count.index)}"]
    }
  }
}
