variable location {}

variable resource_group_name {
  default = ""
  description = "Resource group name"
}

variable lb_public_ip_id {
  default = ""
  description = "Load Balancer public IP"
}

variable sshkey {
  default = ""
  description = "Public key used"
}

variable backend_pool_name {
  default = "MyBackendAddressPool"
  description = "Name of the Load Balancer backend pool"
}

variable "public_ip_address_allocation" {
  description = "Defines how an IP address is assigned, either static or dynamic"
  default = "static"
}

variable "frontend_name" {
  description = "Specifies the name of the frontend ip configuration"
  default = "MyLBFrontEnd"
}

variable "subnet" {
  default = ""
  description = "Default subnet"
}

variable probe_params {
  default = ["MyLBProbe","http","80","/","10","3"]
  description = "List containing LB probe parameters: name, protocol, port, request path, interval in seconds and number of probes"
}

variable rule_params {
  default = ["MyLBRule","tcp","80","80","false","5"]
  description = "List containing LB rule parameters: name, protocol, frontend port, backend port, enable floating IP and idle time in minutes"
}

variable nat_pool_params {
  default = ["3", "MyLBNatPool","tcp","50000","50100","22"]
  description = "List containing LB rule parameters: count, name, protocol, frontend port start, frontend port end, backend port"
}

variable scale_set_params {
  type = "map"
  default = {
    name                = ["MyScaleSet"]
    upgrade_policy_mode = ["Manual"]
    sku                 = ["Standard_DS1_v2","Standard","2"]
    os_profile          = ["myInstance","azureadmin","Fr1ed3ggs!"]
    ssh_keys            = ["/home/azureadmin/.ssh/authorized_keys"]
  }
}
